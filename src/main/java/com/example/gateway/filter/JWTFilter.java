package com.example.gateway.filter;

import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.factory.AbstractNameValueGatewayFilterFactory;
import org.springframework.stereotype.Component;

@Component
public class JWTFilter extends AbstractNameValueGatewayFilterFactory {

    @Override
    public GatewayFilter apply(AbstractNameValueGatewayFilterFactory.NameValueConfig config) {
        System.out.println("-------------------------");
        System.out.println("         jwt filter");
        System.out.println("-------------------------");
        return (exchange, chain) -> chain.filter(exchange);
    }

}
